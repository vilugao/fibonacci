; This is Whitespace assembly.
; Compile this file into Whitespace source code with the asm2ws.lua compiler.

	push 'N
	call 1

	push 'A
	call 1

	push 'B
	call 1

	call 'F
	end

1:	dup
	putc
	push '?
	putc
	push 32
	putc
	getn
	ret

'F:	push 'N
	retr
'I:
	dup
	jz 'B

	push 1
	sub

	dup
	push 'N
	retr
	swap
	sub
	putn

	push '\t
	putc

	push 'A
	retr

	dup
	putn

	push 'B
	retr

	dup
	push 'A
	swap
	stor

	add
	push 'B
	swap
	stor

	push '\n
	putc

	jmp 'I
'B:
	ret
