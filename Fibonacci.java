/* -*- encoding: utf-8 -*- */
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Fibonacci number generator.
 * The Fibonacci number sequence is defined by linear recurrence
 * equation:
 * <blockquote>
 *   <code>F(i) = F(i - 1) + F(i - 2)</code>,
 * </blockquote>
 * where <code>F(0) = 0</code> and <code>F(1) = 1</code>.
 *
 * @author Vinícius Lugão
 */
public final class Fibonacci implements Iterable<Double> {

	/** First term of fibonacci sequence. */
	private double firstTerm;

	/** Second term of fibonacci sequence. */
	private double secondTerm;

	/**
	 * Initializes the Fibonacci sequence using the most commonly
	 * known sequence.
	 * The sequence starts with {@code 0} and {@code 1},
	 * defined as default.
	 *
	 * @see #Fibonacci(double, double)
	 */
	public Fibonacci() {
		this(0, 1);
	}

	/**
	 * Initializes the Fibonacci sequence using your initial sequence.
	 *
	 * @param a the first term of Fibonacci sequence
	 * @param b the second term of Fibonacci sequence
	 * @see #Fibonacci()
	 */
	public Fibonacci(final double a, final double b) {
		firstTerm = a;
		secondTerm = b;
	}

	/**
	 * Gets the first term.
	 *
	 * @return the first term of Fibonacci sequence
	 * @see #setFirstTerm(double)
	 * @see #getSecondTerm()
	 */
	public double getFirstTerm() {
		return firstTerm;
	}

	/**
	 * Modifies the first term.
	 *
	 * @param a the first term of Fibonacci sequence to set
	 * @see #getFirstTerm()
	 * @see #setSecondTerm(double)
	 */
	public void setFirstTerm(final double a) {
		firstTerm = a;
	}

	/**
	 * Gets the second term.
	 *
	 * @return the second term of Fibonacci sequence
	 * @see #setSecondTerm(double)
	 * @see #getFirstTerm()
	 */
	public double getSecondTerm() {
		return secondTerm;
	}

	/**
	 * Modifies the second term.
	 *
	 * @param b the second term of Fibonacci sequence to set
	 * @see #getSecondTerm()
	 * @see #setFirstTerm(double)
	 */
	public void setSecondTerm(final double b) {
		secondTerm = b;
	}

	/**
	 * Returns the {@link Iterator} object to iterate over a Fibonacci
	 * sequence until the overflow is occured.
	 *
	 * @return an {@code Iterator} of {@code Double} type
	 */
	@Override
	public Iterator<Double> iterator() {
		return new Iterator<Double>() {
			private double a = firstTerm;
			private double b = secondTerm;

			/**
			 * Returns {@code true} if this Fibonacci sequence can continue
			 * computing the next term.
			 *
			 * @return {@code true} if the sequence has the next term to be
			 * generated without overflow.
			 * @see #next()
			 */
			@Override
			public boolean hasNext() {
				return !Double.isInfinite(a);
			}

			/**
			 * Returns the current term of Fibonacci sequence and advances.
			 *
			 * @return the current term number in the sequence
			 * @see #hasNext()
			 */
			@Override
			public Double next() {
				final double prev = a;
				a = b;
				b += prev;
				return prev;
			}
		};
	}

	public static void main(final String[] args) {
		final LinkedList<String> argsList
				= new LinkedList<>(Arrays.asList(args));

		final long startMs;
		if (!argsList.isEmpty() && argsList.getFirst().matches("^[-/]?[Tt]$")) {
			argsList.removeFirst();
			startMs = System.currentTimeMillis();
		} else {
			startMs = -1;
		}

		if (argsList.size() > 3) {
			throw new IllegalArgumentException("Too many arguments.");
		}

		final int n = (argsList.size() & 1) == 1
				? Integer.parseInt(argsList.removeFirst()) : -1;
		final Fibonacci fib = argsList.size() == 2
				? new Fibonacci(
						Double.parseDouble(argsList.removeFirst()),
						Double.parseDouble(argsList.removeFirst()))
				: new Fibonacci();

		assert argsList.isEmpty();

		{
			int i = 0;
			for (double x : fib) {
				i++;
				if (n >= 0 && i > n) {
					break;
				}
				System.out.printf("%d\t%G\n", i, x);
			}
		}

		if (startMs != -1) {
			System.out.printf(java.util.Locale.ROOT, "Time: %5.3f s\n",
					(System.currentTimeMillis() - startMs) * 1E-3);
		}
	}
}
