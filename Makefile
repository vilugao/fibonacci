## Compilers ##

CC = gcc
CFLAGS = -ansi -Wall -Wextra -Wmissing-prototypes -O2 -s -march=native

ifeq ($(OS),Win_NT)
CSC = csc
CSFLAGS = /debug- /optimize+
else
CSC = gmcs
CSFLAGS = -debug- -optimize+
endif

JAVAC = javac
JAVAFLAGS = -g:none

LUAC = luac
LUAFLAGS = -s

PYC = pycompile

SCALAC = scalac

AS = as
LD = ld

## Targets ##

TARGETS_C = fibonacci-c
TARGETS_CS = Fibonacci-cs.exe
TARGETS_JAVA = Fibonacci.class
TARGETS_LUA = fibonacci.luac
TARGETS_PY = fibonacci.pyc
TARGETS_SCALA = FibonacciScala.class
TARGETS_DOS = fibonaci.com

ifeq ($(OS),Win_NT)
TARGETS_C := $(TARGETS_C:%=%.exe)
CFLAGS += -mconsole
endif

TARGETS = $(TARGETS_C) \
	$(TARGETS_CS) \
	$(TARGETS_JAVA) \
	$(TARGETS_LUA) \
	$(TARGETS_PY) \
	$(TARGETS_SCALA) \
	$(TARGETS_DOS)

.PHONY: all
all: c cs java lua python scala dos

.PHONY: debug debug-c debug-cs debug-java debug-lua
debug: debug-c debug-cs debug-java debug-lua python scala dos

debug-c: CFLAGS = -Wall -Wextra -Wmissing-prototypes -Og -g -march=native
debug-c: c

ifeq ($(OS),Win_NT)
debug-cs: CSFLAGS = /debug+ /optimize-
else
debug-cs: CSFLAGS = -debug+ -optimize-
endif
debug-cs: cs

debug-java: JAVAFLAGS = -g
debug-java: java

debug-lua: LUAFLAGS =
debug-lua: lua

.PHONY: clean
clean:
	$(RM) $(wildcard $(TARGETS))

## C ##

.PHONY: c
c: $(TARGETS_C)

$(TARGETS_C): fibonacci.c
	$(CC) $(CFLAGS) -o $@ $^

## C# ##

.PHONY: cs
cs: $(TARGETS_CS)

$(TARGETS_CS): Fibonacci.cs
ifeq ($(OS),Win_NT)
	$(CSC) $(CSFLAGS) /out:$@ $^
else
	$(CSC) $(CSFLAGS) -out:$@ $^
endif

## Java ##

.PHONY: java
java: $(TARGETS_JAVA)

%.class: %.java
	$(JAVAC) $(JAVAFLAGS) $^

## Lua ##

.PHONY: lua
lua: $(TARGETS_LUA)

$(TARGETS_LUA): fibonacci.lua
	$(LUAC) $(LUAFLAGS) -o $@ $^

## Python ##

.PHONY: python
python: $(TARGETS_PY)

%.pyc: %.py
	$(PYC) $^

## Scala ##

.PHONY: scala
scala: $(TARGETS_SCALA)

%.class: %.scala
	$(SCALAC) $^

## x86 Assembly - DOS ##

.PHONY: dos
dos: $(TARGETS_DOS)

.INTERMEDIATE: fibonacci-dos.o
$(TARGETS_DOS): fibonacci-dos.o
	$(LD) --oformat binary -Ttext 0x0100 -o $@ $^

%-dos.o: %-dos.s
	$(AS) -R -o $@ $^
