#!/usr/bin/env python
import math
import re
from sys import argv
import time

class Fibonacci:
	def __init__(self, a=0, b=1):
		self.a = a
		self.b = b

	def __iter__(self):
		a, b = float(self.a), float(self.b)
		while not math.isinf(a):
			yield a
			a, b = b, a + b


if __name__ == "__main__":
	clkstart = None
	if len(argv) >= 2 and re.match("^[-/]?[Tt]$", argv[1]) is not None:
		argv.pop(1)
		clkstart = time.clock()

	if len(argv) > 4:
		raise RuntimeError("Too many arguments.")

	n = int(argv.pop(1)) if len(argv) in [2, 4] else None

	fib = Fibonacci()
	if len(argv) == 3:
		fib.a = argv.pop(1)
		fib.b = argv.pop(1)

	assert len(argv) == 1

	for i, x in enumerate(fib, start=1):
		if n is not None and i > n:
			break
		print("%d\t%G" % (i, x))

	if clkstart is not None:
		print("Time: %5.3f s" % (time.clock() - clkstart))
