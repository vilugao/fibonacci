Option Explicit

Class Fibonacci
	Private mFirstTerm, mSecondTerm      ' First and second term
	Private mPreviousTerm, mCurrentTerm  ' Previous and current term

	Private Sub Class_Initialize
		mFirstTerm = 0.0
		mSecondTerm = 1.0
		MoveFirst
	End Sub

	Private Sub MoveFirst
		mPreviousTerm = Empty
		mCurrentTerm = mFirstTerm
	End Sub

	Public Property Let FirstTerm(ByVal x)
		mFirstTerm = CDbl(x)
		MoveFirst
	End Property

	Public Property Get FirstTerm
		FirstTerm = mFirstTerm
	End Property

	Public Property Let SecondTerm(ByVal x)
		mSecondTerm = CDbl(x)
		MoveFirst
	End Property

	Public Property Get SecondTerm
		SecondTerm = mSecondTerm
	End Property

	Public Sub MoveNext
		If IsEmpty(mPreviousTerm) Then
			mPreviousTerm = mFirstTerm
			mCurrentTerm = mSecondTerm
		Else
			Dim prev
			prev = mPreviousTerm
			mPreviousTerm = mCurrentTerm
			mCurrentTerm = prev + mCurrentTerm
		End If
	End Sub

	Public Property Get Current
		Current = mCurrentTerm
	End Property
End Class

''' Main '''

Sub Main
	Dim isWScript   ' wscript.exe (True) or cscript.exe (False) is used?
	Dim iArgs       ' Current argument index.
	Dim dtmStart    ' Time of execution started.
	Dim numTerms    ' Number of fibonacci terms to iterate.
	Dim objFib      ' Fibonacci iterator object.
	Dim i, strOut   ' Loop counter; Output string.

	isWScript = InStr(LCase(WScript.FullName), "wscript.exe") > 0

	If WScript.Arguments.Named.Exists("t") Then
		dtmStart = Now
	End If

	Set objFib = new Fibonacci

	With WScript.Arguments.Unnamed
		Select Case .Count
			Case 0, 2
				numTerms = 32767
				iArgs = 0
			Case 1, 3
				numTerms = CInt(.Item(0))
				iArgs = 1
			Case Else
				If isWScript Then
					MsgBox "Too many arguments.", vbCritical, WScript.Name
				Else
					WScript.StdErr.WriteLine "Too many arguments."
				End If
				WScript.Quit 1
		End Select

		Select Case .Count
			Case 2, 3
				objFib.FirstTerm = .Item(iArgs + 0)
				objFib.SecondTerm = .Item(iArgs + 1)
		End Select
	End With

	For i = 1 To numTerms
		If isWScript Then
			strOut = strOut & i & vbTab & objFib.Current & vbNewLine
		Else
			WScript.Echo i & vbTab & objFib.Current
		End If

		On Error Resume Next
		objFib.MoveNext
		Select Case Err.Number
			Case 0 ' No Error
			Case 6 ' Overflow
				Err.Clear
				Exit For
			Case Else
				WScript.Echo Err.Source & ": " & Err.Description
				WScript.Quit 1
		End Select
		On Error Goto 0
	Next

	If isWScript Then WScript.Echo strOut

	If Not IsEmpty(dtmStart) Then
		WScript.Echo "Time: " & DateDiff("s", dtmStart, Now) & " s"
	End If
End Sub

Main
