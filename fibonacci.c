#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <math.h>

#ifndef isfinite
#define isfinite(x) (fabs(x) != 1.0/0.0)
#endif

struct fibonacci {
	double a;
	double b;
};

int fibonacci_next(struct fibonacci *fib, double *retval);

int fibonacci_next(struct fibonacci * const fib, double * const retval)
{
	const double a = fib->a;
	if (!isfinite(a))
		return 0;
	fib->a = fib->b;
	fib->b += a;
	*retval = a;
	return 1;
}

/* Check if the argstr is a time flag. */
static int isfltime(const char *argstr)
{
	if (argstr[0] == '-' || argstr[0] == '/')
		argstr++;
	return toupper(argstr[0]) == 'T' && argstr[1] == '\0';
}

int main(int argc, const char *argv[])
{
	const char * const progname = argv[0];
	clock_t clkstart = -1;
	int n = -1;
	struct fibonacci fib = { 0.0, 1.0 };

	argv++;
	argc--;

	if (argc > 0 && isfltime(*argv)) {
		clkstart = clock();
		argv++;
		argc--;
	}

	if (argc > 3) {
		fprintf(stderr, "%s: %s\n",
			progname, "Too many arguments.");
		return EXIT_FAILURE;
	}

	if (argc & 1) {
		n = atoi(*argv++);
		argc--;
	}

	if (argc == 2) {
		fib.a = atof(*argv++);
		fib.b = atof(*argv++);
		argc -= 2;
	}

	assert(argc == 0);

	{
		int i;
		double x;
		for (i = 1; (n < 0 || i <= n) && fibonacci_next(&fib, &x); i++)
			printf("%d\t%G\n", i, x);
	}

	if (clkstart != (clock_t)-1)
		printf("Time: %5.3f s\n",
			(clock() - clkstart) / (double)CLOCKS_PER_SEC);

	return EXIT_SUCCESS;
}
