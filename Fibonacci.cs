using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

/// <summary>
/// Fibonacci number generator.
/// </summary>
public class Fibonacci : IEnumerable<double>
{
#region Properties

	/// <summary>
	/// First term to return on next iteration.
	/// </summary>
	/// <seealso cref="SecondTerm"/>
	public double FirstTerm
	{
		get;
		set;
	}

	/// <summary>
	/// Second term to return on next iteration.
	/// </summary>
	/// <seealso cref="FirstTerm"/>
	public double SecondTerm
	{
		get;
		set;
	}

#endregion Properties

#region Initializers

	/// <summary>
	/// Initializes the Fibonacci sequence using the most commonly
	/// known sequence as 0, 1, 1, 2, 3, 5, etc.
	/// </summary>
	/// <seealso cref="Fibonacci(double, double)"/>
	public Fibonacci()
		: this(0.0, 1.0)
	{
	}

	/// <summary>
	/// Initializes the Fibonacci sequence using your initial sequence.
	/// </summary>
	/// <param name="first">First term of Fibonacci sequence.</param>
	/// <param name="second">Second term of Fibonacci sequence.</param>
	/// <seealso cref="Fibonacci()"/>
	public Fibonacci(double first, double second)
	{
		FirstTerm = first;
		SecondTerm = second;
	}

#endregion Initializers

#region Methods

	/// <summary>
	/// Returns an enumerator that iterates through the Fibonacci sequence.
	/// </summary>
	/// <returns>
	/// An <see cref="System.Collections.Generic.IEnumerator&lt;T&gt;"/> object that
	/// can be used to iterate through the Fibonacci sequence.
	/// </returns>
	public IEnumerator<double> GetEnumerator()
	{
		var a = FirstTerm;
		var b = SecondTerm;
		double temp;
		while (!double.IsInfinity(a))
		{
			yield return a;
			temp = a;
			a = b;
			b += temp;
		}
	}

	// Must also implement IEnumerable.GetEnumerator,
	// but implement as a private method.
	private IEnumerator GetEnumerator1()
	{
		return this.GetEnumerator();
	}
	IEnumerator IEnumerable.GetEnumerator()
	{
		return GetEnumerator1();
	}

#endregion Methods

	static void Main(string[] args)
	{
		var argsStack = new Stack<string>(args.Reverse());

		Stopwatch stopwatch = null;
		if (argsStack.Count > 0
			&& Regex.IsMatch(argsStack.Peek(), @"^[-/]?[Tt]$"))
		{
			argsStack.Pop();
			stopwatch = new Stopwatch();
			stopwatch.Start();
		}

		if (argsStack.Count > 3)
		{
			throw new ArgumentException("Too many arguments.");
		}

		var n = argsStack.Count & 1 ? int.Parse(argsStack.Pop())
		                            : int.MaxValue;
		var fib = argsStack.Count == 2 ?
				new Fibonacci(double.Parse(argsStack.Pop()),
				              double.Parse(argsStack.Pop())) :
				new Fibonacci();

		Debug.Assert(argsStack.Count == 0);

		{
			var index = 0;
			foreach (double next in fib.Take(n))
			{
				Console.WriteLine("{0}\t{1}", ++index, next);
			}
		}

		if (stopwatch != null)
		{
			stopwatch.Stop();
			Console.WriteLine("Time: {0:F3} s",
				stopwatch.ElapsedMilliseconds * 1E-3);
		}
	}
}
