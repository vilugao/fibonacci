# Fibonacci generator program

This repository is a collection of Fibonacci sequence generators,
written in many of the popular programming languages.

Each file in this repository contains one Fibonacci generator
and the main part that reads the user input and prints the Fibonacci sequence.

## Rules

 -	Create only one file for each programming language,
	with files named as `fibonacci.xyz`,
	where `.xyz` is a placeholder of file extension
	for particular programming language.
	Can be in uppercase or CamelCase, only when required.
	Append the file name with specific language name or platform,
	when required to operate correctly.
 -	Use the generator function for sequencing the Fibonacci.
	If such programming languages lack its native support,
	create a function that provides one for each term of sequence
	and returns the done status,
	simulating a generator function for use with the `while` loop.
	See [`fibonacci.c`](fibonacci.c) for example.
 -	The Fibonacci generator function should use the two variables: A and B,
	where A and B are the first and the second term of sequence,
	respectively.
	The default values for A and B should be zero and one, respectively.
 -	If the term to be returned by generator function is a number much larger
	that a program cannot store and display correctly,
	known as the overflow condition,
	then the generator should return the status as done
	and should stop providing the next terms.
 -	For object oriented programming languages,
	implement a Fibonacci class that uses the default `a = 0` and `b = 1`,
	and have an iterator method for use with the `for (x in f)`.
	The iterator method ideally never change the two instance variables of
	`a` and `b` initial terms.
 -	Document the source code using the standard documentation comments
	for that programming language,
	as the Javadoc for the source codes written in Java.
 -	Try to follow the standard code conventions
	for that programming language used.

See [`Fibonacci.java`](Fibonacci.java) and
[`Fibonacci.cs`](Fibonacci.cs),
for examples of object oriented Fibonacci implementation.
See [`fibonacci.c`](fibonacci.c) for reference Fibonacci implementation,
lacking some of the programming features.
See [`fibonacci.lua`](fibonacci.lua),
[`fibonacci.rb`](fibonacci.rb) and
[`fibonacci.py`](fibonacci.py)
for examples of scripting Fibonacci implementation.

### Demo app

 -	Each file should be executable, putting the main logic
	for demonstration purposes.
 -	These apps should receive the three optional parameters in that order:
	N, A and B;
	where N is the quantity of terms to display, and
	A and B are the first and the second term of sequence, respectively.
 -	The app should show results on the console output.
 -	If the arguments entered by user has more than necessary (> 3),
	print the error message "Too many arguments." and exit.
 -	Include the timing elapsed on main program
	when that programming language has it support.
	If supported, implement the argument switch
	that enables displaying the total time elapsed
	to generate the Fibonacci sequence.
	The optional time switch should be received at first argument,
	before the N, A and B inputs.

See **fibonacci**(1) man page in this package for details on executing the app.

#### Standard input alternative

If the app arguments cannot be used as a user input,
then should be received by a standard console input.
The app should read a console input one line at a time
and terminate when receives the end-of-file (EOF) signal.

## Command-line usage

	> fibonacci [-t|-T|/t|/T|t|T] [<N>] [<A> <B>]

 - `[-t|-T|/t|/T|t|T]`: Show the time elapsed (optional);
 - `[<N>]`: the quantity of terms to display (default: infinity);
 - `[<A> <B>]`: the first and the second term of sequence (default: 0 1).

## Examples

	> fibonacci 10
	1	0
	2	1
	3	1
	4	2
	5	3
	6	5
	7	8
	8	13
	9	21
	10	34

	> fibonacci 10 2 3
	1	2
	2	3
	3	5
	4	8
	5	13
	6	21
	7	34
	8	55
	9	89
	10	144

## To do

 -	Implement the Fibonacci generator version for
	Swift,
	Go,
	R,
	Visual Basic (.Net),
	Delphi,
	Dart,
	D,
	COBOL and
	Fortran
	programming languages when possible and applicable.
	These are the top 30 popular languages
	ranked by [Tiobe Index](https://www.tiobe.com/tiobe-index/).
 -	Evolve the Bash version to fully follow the rules above.

Your suggestions or contributions are welcome.
On GitHub, use the Issues page if you have suggestions or bugs found in code,
use the Pull request if you have a complete code to add or to fix some bugs.
