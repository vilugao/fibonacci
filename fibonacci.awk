#!/usr/bin/awk -f
function print_fibonacci(n, a, b,  c, i) {
	i = 1
	while ((n < 0 || n-- > 0) && (a < 0 ? -a : a) != "inf") {
		print(i++, a)
		c = a + b
		a = b
		b = c
	}
}

NF == 1 && $1 ~ /^[0-9]+$/ {
	print_fibonacci($1, 0, 1)
}

NF == 2 && $1 ~ /^-?[0-9]+$/ && $2 ~ /^-?[0-9]+$/ {
	print_fibonacci(-1, $1, $2)
}

NF == 3 && $1 ~ /^[0-9]+$/ && $2 ~ /^-?[0-9]+$/ && $3 ~ /^-?[0-9]+$/ {
	print_fibonacci($1, $2, $3)
}
