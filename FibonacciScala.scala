class FibonacciScala(var firstTerm: Double, var secondTerm: Double)
extends Iterable[Double] {

	def this() = this(0.0, 1.0)

	override def iterator: Iterator[Double] = {
		Iterator.iterate((firstTerm, secondTerm)) {
			case (a, b) => (b, a + b)
		}.map { case (a, _) => a }.takeWhile(a => !a.isInfinite)
	}
}

object FibonacciScala extends App {
	val argsBuffer = args.toBuffer

	val startMs =
		if (argsBuffer.nonEmpty && argsBuffer(0).matches("^[-/]?[Tt]$")) {
			argsBuffer.remove(0)
			System.currentTimeMillis
		} else {
			-1
		}

	if (argsBuffer.length > 3) {
		throw new IllegalArgumentException("Too many arguments.")
	}

	val n = argsBuffer.length match {
			case 1 | 3 => argsBuffer.remove(0).toInt
			case _ => Int.MaxValue
		}

	val fib = new FibonacciScala
	if (argsBuffer.length == 2) {
		fib.firstTerm = argsBuffer.remove(0).toDouble
		fib.secondTerm = argsBuffer.remove(0).toDouble
	}

	assert(argsBuffer.isEmpty)

	for ((x, i) <- fib.take(n).zipWithIndex) {
		printf("%d\t%G\n", i + 1, x)
	}

	if (startMs != -1) {
		printf(
			"Time: %5.3f s\n",
			(System.currentTimeMillis - startMs) * 1E-3)
	}
}
