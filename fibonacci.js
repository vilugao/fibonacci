/* eslint-env browser, node */
/* jshint es3: true, browser: true, node: true */
/* global WScript */

/**
 * Constructor of Fibonacci class.
 * @constructor
 * @param {number} [a=0] - The first term of Fibonacci sequence.
 * @param {number} [b=1] - The second term of Fibonacci sequence.
 */
function Fibonacci(a, b) {
	"use strict";

	/** First term of fibonacci sequence. */
	this.firstTerm = a || 0;

	/** Second term of fibonacci sequence. */
	this.secondTerm = b || 1;
}

/**
 * Gets the fibonacci iterator.
 * <p>
 * The iterator object has the <code>next()</code> function, which returns an
 * object with the properties <code>done</code> and <code>value</code>.
 * The <code>done</code> property has a boolean value indicating if this
 * iterator is over.
 * The <code>value</code> property returns the current value.
 *
 * @returns {Object} A Fibonacci's iterator object implementing the
 * <code>next()</code> function.
 */
Fibonacci.prototype.iterator = function() {
	"use strict";

	var a = this.firstTerm;
	var b = this.secondTerm;

	return {
		next: function() {
			if (!isFinite(a)) {
				return { done: true };
			}
			var value = a;
			a = b;
			b += value;
			return { done: false, value: value };
		}
	};
};

/**
 * The main function.
 *
 * @param {string[]} args - The command line arguments.
 * @param {function} print - The function to print the results.
 */
function main(args, print) {
	"use strict";

	var startTime;
	if (args.length >= 1 && (/^[-/]?t$/i).test(args[0])) {
		args.shift();
		startTime = new Date().getTime();
	}

	if (args.length > 3) {
		throw "Too many arguments.";
	}

	var n = args.length % 2 ? parseInt(args.shift(), 10)
	                        : Number.POSITIVE_INFINITY;
	var fib = args.length === 2 ?
			new Fibonacci(parseFloat(args.shift()),
			              parseFloat(args.shift())) :
			new Fibonacci();

	if (args.length !== 0) {
		throw "Assertion failed!";
	}

	var i = 0;
	fib = fib.iterator();
	for (var data = fib.next(); !data.done; data = fib.next()) {
		i += 1;
		if (i > n) {
			break;
		}
		print(i + "\t" + data.value);
	}

	if (typeof startTime === "number") {
		startTime = (new Date().getTime() - startTime) * 1e-3;
		print("Time: " + startTime.toFixed(3) + " s");
	}
}

/* ================================================================== */
/* eslint-disable require-jsdoc */

function getPrinter() {
	"use strict";

	/* Windows Script Host */
	if (typeof WScript === "object") {
		return function (msg) {
			WScript.Echo(msg);
		};
	}

	/* Javascript in browser */
	if (
		typeof document === "object" &&
		document.forms !== undefined &&
		document.forms.fibonacci !== undefined
	) {
		return function (msg) {
			if (typeof console === "object" && console.log !== undefined) {
				console.log(msg);
			}
			document.forms.fibonacci.elements.result.value += msg + "\n";
		};
	}

	/* Node.js */
	if (typeof console === "object" && console.log !== undefined) {
		return console.log;
	}

	/* Rhino Shell */
	return (typeof print === "function") ? print : null;
}

function getArgs(args) {
	"use strict";
	var i;

	/* Windows Script Host */
	if (typeof WScript === "object") {
		args = new Array(WScript.Arguments.Length);
		for (i = 0; i < WScript.Arguments.Length; i += 1) {
			args[i] = WScript.Arguments.Item(i);
		}
		return args;
	}

	/* Node.js */
	if (typeof process === "object" && process.argv !== undefined) {
		return process.argv.slice(2);
	}

	/* Javascript in browser */
	if (
		typeof location === "object" &&
		typeof location.search === "string"
	) {
		return (function() {
			var queries = {};
			var queryString = location.search.substring(1);
			var re = /([^&]+)=([^&]*)/g;
			var matches = re.exec(queryString);
			while (matches !== null) {
				queries[matches[1]] = matches[2];
				matches = re.exec(queryString);
			}

			var newArgs = [];
			if (typeof queries.a === "string") {
				newArgs = [queries.a, queries.b];
			}
			if (typeof queries.n === "string") {
				newArgs.unshift(queries.n);
			}
			if (newArgs.length === 0) {
				return ["0"];
			}
			return newArgs;
		}());
	}

	/* Rhino Shell */
	return args;
}

(function(args) {
	"use strict";
	var printer = getPrinter();

	if (
			typeof document === "object" &&
			document.forms !== undefined &&
			document.forms.fibinput !== undefined
	) {
		document.forms.fibinput.onsubmit = function() {
			var e = document.forms.fibinput.elements;
			document.forms.fibonacci.elements.result.value = "";
			var newArgs = [];
			if (e.a.value.length > 0) {
				newArgs = [e.a.value, e.b.value];
			}
			if (e.n.value.length > 0) {
				newArgs.unshift(e.n.value);
			}
			main(newArgs, printer);
			return false;
		};
	}

	main(getArgs(args), printer);
}(typeof arguments === "object" ? /* Rhino Shell */ arguments : null));
