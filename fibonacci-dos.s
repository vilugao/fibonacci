/* Compile:
 *   as -R -o fibonaci.dos.o fibonaci.dos.s
 *   ld --oformat binary -Ttext 0x0100 -o fibonaci.com fibonaci.dos.o
 */
	.arch	i8086
	.code16

	.text
	.globl	_start
_start:
parse_args:
	xor	%ch, %ch
	mov	0x80, %cl
	mov	$0x81, %si
	call	skipspaces
	jcxz	0f

	cmp	$'/, %al
	je	2f
	cmp	$'-, %al
	jne	3f

2:	dec	%cx
	jcxz	fail_too_many_args
	inc	%si
	mov	(%si), %al

3:	cmp	$'T, %al
	je	4f
	cmp	$'t, %al
	jne	1f

4:	inc	%si
	dec	%cx
	movb	$1, flag_show_time

1:	call	skipspaces
	jcxz	0f

	call	read_next_uint16
	jcxz	1f
	mov	%ax, fiba

	call	read_next_uint16
	jcxz	2f

	mov	fiba, %dx	/* Passa o valor de fiba para fibn. */
	test	%dh, %dh
	jnz	fail_n_large
	mov	%dl, fibn
	mov	%ax, fiba	/* Passa o último valor lido para fiba. */

	call	read_next_uint16
	jcxz	2f

fail_too_many_args:
	mov	$msg_too_many_args, %dx
	mov	$9, %ah		/* WRITE STRING TO STDOUT */
	int	$0x21
	jmp	fail

2:	mov	%ax, fibb
	jmp	0f

1:	test	%ah, %ah
	jnz	fail_n_large
	mov	%al, fibn
0:

savetime:
	cmpb	$0, flag_show_time
	je	0f
	mov	$0x2c, %ah	/* GET CURRENT TIME */
	int	$0x21
	mov	%dx, stsc
0:

fibonacci:
	xor	%cx, %cx
	mov	fiba, %dx

1:	cmp	%cl, fibn
	je	0f
	inc	%cx
	call	print_fibonacci
	call	fibonacci_next
	jnc	1b

	cmp	%cl, fibn	/* Print the last number */
	je	0f
	inc	%cx
	call	print_fibonacci
0:

show_time:
	cmpb	$0, flag_show_time
	je	0f
	mov	$msg_time, %dx
	mov	$9, %ah		/* WRITE STRING TO STDOUT */
	int	$0x21

	mov	$0x2c, %ah	/* GET CURRENT TIME */
	int	$0x21
	mov	stsc, %ax
	cmp	%ax, %dx
	je	1f

	sub	%ah, %dh	/* Calculate the time diff */
	jns	2f
	add	$60, %dh
2:	sub	%al, %dl
	jns	2f
	add	$100, %dl
2:
	mov	%dh, %al	/* Convert the time diff to centiseconds */
	xor	%ah, %ah
	mov	$100, %bl
	xor	%dh, %dh
	mul	%bl
	add	%dx, %ax

	call	print_uint16

1:	mov	$msg_time_end, %dx
	mov	$9, %ah		/* WRITE STRING TO STDOUT */
	int	$0x21
0:
	xor	%al, %al

_exit:
	mov	$0x4c, %ah
	int	$0x21		/* TERMINATE PROGRAM */
1:	hlt
	jmp	1b

fail_n_large:
	mov	$msg_n_too_large, %dx
	mov	$9, %ah		/* WRITE STRING TO STDOUT */
	int	$0x21
fail:
	mov	$1, %al
	jmp	_exit

/**
 * Skips the spaces begining in a string.
 * Inputs:
 *  - CX: Length of string.
 *  - SI: String pointer.
 * Returns:
 *  - AL: Last byte string scanned, if length > 0.
 *  - CX: Final length.
 *  - SI: Final string pointer.
 */
skipspaces:
	jcxz	0f

1:	mov	(%si), %al
	cmp	$0x20, %al
	je	2f
	cmp	$'\t, %al
	jne	0f
2:	inc	%si
	loop	1b

0:	ret

/**
 * Reads the decimal number in a string.
 * Inputs:
 *  - CX: Length of string.
 *  - SI: String pointer.
 * Returns:
 *  - AX: Value read.
 *  - CX: Final length.
 *  - SI: Final string pointer.
 */
read_next_uint16:
	xor	%ax, %ax
	jcxz	0f
	push	%dx
	xor	%dh, %dh

1:	mov	(%si), %dl
	sub	$'0, %dl
	cmp	$9, %dl
	ja	1f
	push	%dx
	mov	$10, %dx
	mul	%dx
	pop	%dx
	jc	fail_n_large
	add	%dx, %ax
	jc	fail_n_large
	inc	%si
	loop	1b

1:	pop	%dx
	push	%ax
	call	skipspaces
	pop	%ax
0:	ret

/**
 * Prints two numbers separated by tab, CX and DX.
 * Inputs:
 *  - CX: Iteration number.
 *  - DX: Fibonacci term.
 * Returns: None.
 */
print_fibonacci:
	push	%ax
	push	%dx

	mov	%cx, %ax
	call	print_uint16

	push	%dx
	mov	$2, %ah		/* WRITE CHAR TO STDOUT */
	mov	$'\t, %dl
	int	$0x21
	pop	%dx

	mov	%dx, %ax
	call	print_uint16

	mov	$newline_print, %dx
	mov	$9, %ah		/* WRITE STRING TO STDOUT */
	int	$0x21

	pop	%dx
	pop	%ax
	ret

/**
 * Prints the AX register content in base 10.
 * Inputs:
 *  - AX: value to print.
 * Returns: None.
 */
print_uint16:
	push	%ax
	push	%cx
	push	%dx
	push	%di

	mov	$end_str_uint16, %di
	mov	$10, %cx

1:	xor	%dx, %dx
	div	%cx
	add	$'0, %dl
	dec	%di
	mov	%dl, (%di)
	test	%ax, %ax
	jnz	1b

	mov	$9, %ah		/* WRITE STRING TO STDOUT */
	mov	%di, %dx
	int	$0x21

	pop	%di
	pop	%dx
	pop	%cx
	pop	%ax
	ret

/**
 * Executes fiba, fibb = fibb, fiba + fibb.
 * Inputs: None.
 * Returns:
 *  - AX: fiba previous value.
 *  - DX: fiba current value.
 * CF is set if unsigned overflow.
 */
fibonacci_next:
	mov	fiba, %ax
	mov	fibb, %dx
	mov	%dx, fiba
	add	%ax, fibb
	ret

	.data
	.align	4
stsc:
	/* Start sec/centiseconds */
	.word	0
fiba:
	.word	0
fibb:
	.word	1
fibn:
	.byte	-1
flag_show_time:
	.byte	0

str_uint16:
	.ascii	"00000"
end_str_uint16:
	.ascii	"$"

msg_n_too_large:
	.ascii	"Number too big.\r\n$"
msg_too_many_args:
	.ascii	"Too many arguments."
newline_print:
	.ascii	"\r\n$"

msg_time:
	.ascii	"Time: $"
msg_time_end:
	.ascii	"0 ms\r\n$"
