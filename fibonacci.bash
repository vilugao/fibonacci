#!/usr/bin/env bash
print_fibonacci() {
	local n=$1
	local a=$2
	local b=$3
	for ((i = 1; i <= n; i++)); do
		printf "%d\t%G\n" $i $a
		local c=$((a + b))
		a=$b
		b=$c
	done
}

# TODO: Implement time switch invoking logic.

case $# in
0)
	# TODO: Implement "infinite" fibonacci sequence.
	print_fibonacci -1 0 1
	;;
1)
	print_fibonacci $1 0 1
	;;
2)
	# TODO: Implement "infinite" fibonacci sequence.
	print_fibonacci -1 $1 $2
	;;
3)
	print_fibonacci $1 $2 $3
	;;
*)
	echo "${0}: Too many arguments." > /dev/stderr
	exit 1
	;;
esac
