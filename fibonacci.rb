#!/usr/bin/env ruby
class Fibonacci
	attr_accessor :a, :b

	def initialize(a = 0.0, b = 1.0)
		@a, @b = a, b
	end

	def each
		Enumerator.new do |y|
			a, b = @a.to_f, @b.to_f
			until a.infinite?
				y << a
				a, b = b, a + b
			end
		end
	end
end

if __FILE__ == $0
	start_time =
		if !ARGV.empty? && ARGV[0] =~ /^[-\/]?[Tt]$/
			ARGV.shift
			Time.now
		end

	raise "Too many arguments." if ARGV.length > 3

	n =
		case ARGV.length
		when 1, 3 then ARGV.shift.to_i
		end

	fib = Fibonacci.new
	if ARGV.length == 2
		fib.a = ARGV.shift.to_f
		fib.b = ARGV.shift.to_f
	end

	raise "Assertion error: More arguments not consumed" unless ARGV.empty?

	fib.each.with_index(1) do |x, i|
		break if n && i > n
		printf("%d\t%G\n", i, x)
	end

	printf("Time: %5.3f s\n", Time.now - start_time) if start_time
end
