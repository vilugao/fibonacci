#!/usr/bin/env lua
function fibonacci(a, b)
	local function isinf(x)
		return math.abs(x) == 1/0
	end

	local i = 0
	a, b = a or 0, b or 1

	return function ()
		if not isinf(a) then
			local prev = a
			a, b = b, a + b
			i = i + 1
			return i, prev
		end
	end
end

function main()
	local clkstart
	if arg[1] and arg[1]:find("^[-/]?[Tt]$") then
		table.remove(arg, 1)
		clkstart = os.clock()
	end

	if #arg > 3 then
		error "Too many arguments."
	end

	local n = math.huge
	if #arg % 2 == 1 then
		n = tonumber(table.remove(arg, 1), 10)
	end

	local a, b
	if #arg == 2 then
		a = tonumber(table.remove(arg, 1))
		b = tonumber(table.remove(arg, 1))
	end

	assert(#arg == 0)

	for i, x in fibonacci(a, b) do
		if i > n then
			break
		end
		print(i, x)
	end

	if clkstart then
		print(string.format("Time: %5.3f s", os.clock() - clkstart))
	end
end

main()
